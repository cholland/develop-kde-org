#
# Shinjo Park <kde@peremen.name>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-25 00:38+0000\n"
"PO-Revision-Date: 2022-05-07 14:52+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: config.yaml:0
msgid "API"
msgstr "API"

#: i18n/en.yaml:0
msgid "Previous"
msgstr "이전"

#: i18n/en.yaml:0
msgid "Next"
msgstr "다음"

#: i18n/en.yaml:0
msgid "Read more"
msgstr "자세한 내용 보기"

#: i18n/en.yaml:0
msgid "Search this site…"
msgstr "이 사이트 검색…"

#: i18n/en.yaml:0
msgid "in"
msgstr ""

#: i18n/en.yaml:0
msgid "All Rights Reserved"
msgstr "All Rights Reserved"

#: i18n/en.yaml:0
msgid "Privacy Policy"
msgstr "개인 정보 처리 방침"

#: i18n/en.yaml:0
msgid "By"
msgstr ""

#: i18n/en.yaml:0
msgid "Created"
msgstr "만든 날짜"

#: i18n/en.yaml:0
msgid "Last modified"
msgstr "수정한 날짜"

#: i18n/en.yaml:0
msgid "Edit this page"
msgstr "이 페이지 편집하기"

#: i18n/en.yaml:0
msgid "Create documentation issue"
msgstr "문서 이슈 만들기"

#: i18n/en.yaml:0
msgid "Create project issue"
msgstr "프로젝트 이슈 만들기"

#: i18n/en.yaml:0
msgid "Posts in"
msgstr ""

#: i18n/en.yaml:0
msgid "Icons"
msgstr "아이콘"

#: i18n/en.yaml:0
msgid "Categories"
msgstr "분류"

#: i18n/en.yaml:0
msgid "Search for icons"
msgstr "아이콘 검색"

#: i18n/en.yaml:0
msgid "Start typing to filter..."
msgstr ""

#: i18n/en.yaml:0
msgid "Nothing found for this category/keyword combination."
msgstr ""

#: i18n/en.yaml:0
msgid "KDE Developer Platform"
msgstr "KDE 개발 플랫폼"

#: i18n/en.yaml:0
msgid ""
"Design, build and distribute beautiful, usable applications with KDE "
"technologies."
msgstr ""
"KDE 기술을 사용하여 아름답고 사용하기 편한 프로그램을 설계, 개발, 배포하십시"
"오."

#: i18n/en.yaml:0
msgid "Multi-Platform"
msgstr "멀티 플랫폼"

#: i18n/en.yaml:0
msgid ""
"Develop once, deploy everywhere. Built on top of Qt, KDE's technologies work "
"on every platform."
msgstr ""
"한 번 개발하고 여러 곳에 배포하십시오. Qt를 기반으로 하는 KDE 기술은 어느 플"
"랫폼에서도 작동합니다."

#: i18n/en.yaml:0
msgid "Desktop Linux, Android, Windows, macOS, embedded, and more"
msgstr "데스크톱 리눅스, Android, Windows, macOS, 임베디드, 그리고 더"

#: i18n/en.yaml:0
msgid "Plasma running on a phone, laptop and TV"
msgstr "휴대폰, 노트북, TV에서 실행 중인 Plasma"

#: i18n/en.yaml:0
msgid "KDE Frameworks: Enhance the Qt Experience"
msgstr "KDE 프레임워크: Qt 경험 강화"

#: i18n/en.yaml:0
msgid ""
"KDE Frameworks cover 80 add-on libraries for programming with Qt. All "
"libraries have been well-tested in real world scenarios and are "
"comprehensively documented. KDE's libraries are distributed under LGPL or "
"MIT licenses."
msgstr ""
"KDE 프레임워크는 Qt 프로그래밍에 사용할 수 있는 80여 개의 추가 기능 라이브러"
"리입니다. 모든 라이브러리는 실제 상황에서 사용할 수 있도록 테스트되었으며 모"
"든 부분이 문서화되어 있습니다. KDE 라이브러리는 LGPL이나 MIT 라이선스로 배포"
"됩니다."

#: i18n/en.yaml:0
msgid "Kirigami UI Framework"
msgstr "Kirigami UI 프레임워크"

#: i18n/en.yaml:0
msgid "Kirigami"
msgstr "Kirigami"

#: i18n/en.yaml:0
msgid ""
"Build Beautiful, Convergent Apps that Run on Phones, TVs and Everything in "
"Between."
msgstr ""
"휴대폰, TV, 그 사이에 있는 모든 것에서 실행되는 아름답고 통합된 앱을 개발하십"
"시오."

#: i18n/en.yaml:0
msgid ""
"The line between desktop and mobile is blurring and users expect the same "
"quality experience on every device. Applications using Kirigami adapt "
"brilliantly to mobile, desktop, TVs, infortainment systems and everything in "
"between."
msgstr ""
"데스크톱과 모바일 사이의 경계선은 점점 흐려지고 있으며, 사용자들은 모든 장치"
"에서 비슷한 사용자 경험을 기대하고 있습니다. Kirigami를 사용하는 프로그램은 "
"모바일, 데스크톱, TV, 인포테인먼트 시스템뿐만 아니라 그 사이에 있는 어떤 것에"
"도 융합됩니다."

#: i18n/en.yaml:0
msgid ""
"Kirigami's components are goodlooking and consistent, and Kirigami itself "
"provides a clearly defined workflow. Users of Kirigami apps will appreciate "
"the smart choices made in the API and uncluttered design."
msgstr ""
"Kirigami의 구성 요소는 미적인 요소뿐만 아니라 일관성도 고려되었으며, "
"Kirigami 자체적으로도 작업의 흐름을 정합니다. Kirigami 앱을 사용해 보았다면 "
"API 설계 방식과 번잡하지 않은 디자인을 높게 살 것입니다."

#: i18n/en.yaml:0
msgid "Discover Kirigami"
msgstr "Kirigami 둘러보기"

#: i18n/en.yaml:0
msgid "KDevelop"
msgstr "KDevelop"

#: i18n/en.yaml:0
msgid "A cross-platform IDE for C, C++, Python, QML/JavaScript and PHP"
msgstr ""
"C, C++, Python, QML/자바스크립트, PHP 등을 지원하는 크로스 플랫폼 통합 개발 "
"환경"

#: i18n/en.yaml:0
msgid ""
"Open Source, powerful and fast, KDevelop  offers a seamless development "
"environment to programmers that work on projects of any size. KDevelop helps "
"you get the job done while staying out of your way."
msgstr ""
"오픈 소스, 강력함, 빠름. KDevelop은 어떠한 크기의 프로젝트라도 편하게 개발할 "
"수 있는 통합 개발 환경을 제공합니다. KDevelop을 사용하여 집중을 잃지 않고 작"
"업을 완료할 수 있습니다."

#: i18n/en.yaml:0
msgid "Get KDevelop"
msgstr "KDevelop 설치"

#: i18n/en.yaml:0
msgid "Documentation"
msgstr "문서"

#: i18n/en.yaml:0
msgid ""
"From beginners to experienced Qt developers, here is all you will need to "
"know to start developing KDE applications."
msgstr ""
"초보자부터 전문 Qt 개발자까지 모두가 참조할 수 있는 KDE 프로그램 개발을 시작"
"할 때 필요한 자료입니다."

#: i18n/en.yaml:0
msgid "Design"
msgstr "디자인"

#: i18n/en.yaml:0
msgid "Learn about KDE's design guidelines for apps and icons"
msgstr "KDE의 앱과 아이콘 디자인 가이드라인 알아보기"

#: i18n/en.yaml:0
msgid "Develop"
msgstr "개발"

#: i18n/en.yaml:0
msgid "Check out the tools and libraries that help you build KDE apps"
msgstr "KDE 앱을 만들 때 필요한 도구와 라이브러리 알아보기"

#: i18n/en.yaml:0
msgid "Distribute"
msgstr "배포"

#: i18n/en.yaml:0
msgid "Get your application in front of users"
msgstr "내 프로그램을 사용자에게 배포하기"
